public with sharing class RecordCreatorEditorController {

    private static Map<Schema.DisplayType, String> inputTypeByDisplayType = new Map<Schema.DisplayType, String>{
        Schema.DisplayType.BOOLEAN => 'checkbox',
        Schema.DisplayType.CURRENCY => 'number',
        Schema.DisplayType.DATE => 'date',
        Schema.DisplayType.DATETIME => 'datetime',
        Schema.DisplayType.DOUBLE => 'number',
        Schema.DisplayType.EMAIL => 'email',
        Schema.DisplayType.INTEGER => 'number',
        Schema.DisplayType.PERCENT => 'number',
        Schema.DisplayType.PHONE => 'tel',
        Schema.DisplayType.URL => 'url'
    };

    private static String MTD_RECORD_NAME = 'Tester';

    private static Set<String> blacklistedFields = new Set<String>{'CompletedDateTime','RecurrenceRegeneratedType','RecurrenceMonthOfYear','RecurrenceInstance','RecurrenceDayOfMonth','RecurrenceDayOfWeekMask','RecurrenceType','RecurrenceTimeZoneSidKey','RecurrenceEndDateOnly','RecurrenceStartDateOnly  ','RecurrenceActivityId','ReminderDateTime','ActivityOriginType','ArchivedDate','IsArchived','IsClosed','PrioritySortOrder','IsHighPriority','AccountId','Jigsaw Contact Id','ReportsToName','EmailBouncedReason','EmailBouncedDate','IsEmailBounced','JigsawContactId','Id','IsDeleted','CreatedDate','LastModifiedDate','SystemModstamp','LastActivityDate','LastViewedDate','LastReferencedDate','UserRecordAccessId','MasterRecordId','AccountSource','IsCssEnabled','CssLastLoginDate','CompareName','PhotoUrl','CompareSite','OwnerAlias','JigSawCompanyId','ConnectionReceivedDate','ConnectionSentDate','AccountRollupId','ProductIsArchived', 'OwnerId'};     

    @AuraEnabled(cacheable=true)
    public static List<ObjectSearchResult> getAvailableObjects()
    {
        System.debug('START getAvailableObjects');
        List<ObjectSearchResult> result = new List<ObjectSearchResult>();
        List<EntityDefinition> entities = [SELECT QualifiedApiName, Label
                                            FROM EntityDefinition
                                            WHERE IsQueryable = True
                                            ORDER BY Label ASC];
        for(EntityDefinition eachEntity : entities) {
            result.add(new ObjectSearchResult(eachEntity));
        }
        System.debug(result);
        return result;
    }

    @AuraEnabled(cacheable=true)
    public static FieldResponse getReadableObjectFields(String objectApiName)
    {
        System.debug('START getReadableObjectFields ' + objectApiName);
        FieldResponse response = new FieldResponse();

        System.debug('Get previous config');
        Map<String, Object> previousConfig = getPreviousConfig(objectApiName);

        // Get field map for object
        System.debug('Get field map');
        SObjectType recordType = ((SObject)(Type.forName('Schema.'+objectApiName).newInstance())).getSObjectType(); // See: https://salesforce.stackexchange.com/questions/218982/why-is-schema-describesobjectstypes-slower-than-schema-getglobaldescribe
        Map<String,Schema.SObjectField> fieldMap = recordType.getDescribe().fields.getMap();

        List<FieldData> fieldData = new List<FieldData>();
        for (Schema.SObjectField eachField : fieldMap.values())
        {
            Schema.DescribeFieldResult field = eachField.getDescribe();
            System.debug('field: ' + field.getName());
            if(field.isAccessible() && field.isCreateable() && !blacklistedFields.contains(field.getName())) {
                FieldData newField = new FieldData(field);
                String inputType = inputTypeByDisplayType.get(field.getType()) != null ? inputTypeByDisplayType.get(field.getType()) : 'text';
                newField.inputType = inputType;
                if(previousConfig != null) {
                    Object previousValue = previousConfig.get(field.getName());
                    newField.previousValue = previousValue;
                }
                fieldData.add(newField);
            }
        }
        fieldData.sort();

        response.fields = fieldData;
        return response;
    }

    @AuraEnabled
    public static void updateConfig(String objectName, String fieldsJsonStr) {
        System.debug('START updateConfig');
        System.debug(objectName);
        System.debug(fieldsJsonStr);

        Mandatory_Field__mdt metadataConfig = [SELECT Id, Field_Data__c FROM Mandatory_Field__mdt WHERE QualifiedApiName  = :MTD_RECORD_NAME];
        String previousJSON = metadataConfig.Field_Data__c;
        Map<String, Object> entireConfig = (Map<String, Object>) JSON.deserializeUntyped(previousJSON);
        entireConfig.put(objectName, fieldsJsonStr);
        String updatedConfig = JSON.serialize(entireConfig);
        updatedConfig = updatedConfig.replace('\\', '');
        updatedConfig = updatedConfig.replace('"{', '{');
        updatedConfig = updatedConfig.replace('}"', '}');
        System.debug(updatedConfig);
        metadataConfig.Field_Data__c = updatedConfig;

        Map<String, Object> metadataFieldValueMap = new Map<String, Object>();
        metadataFieldValueMap.put('Field_Data__c', updatedConfig);
        // CustomMetadataUtils.createCustomMetadata('Mandatory_Field__mdt', 'Test Deploy', metadataFieldValueMap);
        // CustomMetadataUtils.updateCustomMetadata('Mandatory_Field__mdt', 'Test_Deploy', 'Test Deploy', metadataFieldValueMap);
        CustomMetadataUtils.updateCustomMetadata('Mandatory_Field__mdt', MTD_RECORD_NAME, MTD_RECORD_NAME, metadataFieldValueMap);
        System.debug('END updateConfig');
    }

    private static Map<String, Object> getPreviousConfig(String objectApiName) {

        // String previousJSON = [SELECT Id, Field_Data__c FROM Mandatory_Field__mdt WHERE QualifiedApiName  = 'Test_Deploy'].Field_Data__c;
        String previousJSON = [SELECT Id, Field_Data__c FROM Mandatory_Field__mdt WHERE QualifiedApiName  = :MTD_RECORD_NAME].Field_Data__c;
        Map<String, Object> o = (Map<String, Object>) JSON.deserializeUntyped(previousJSON);
        Map<String, Object> result = (Map<String, Object>) o.get(objectApiName);
        return result;
        // System.debug(fieldValueByFieldName);

        // SObjectType recordType = ((SObject)(Type.forName('Schema.'+objectApiName).newInstance())).getSObjectType();
        // Map<String,Schema.SObjectField> fieldMap = recordType.getDescribe().fields.getMap();
        // for(String eachFieldName : fieldValueByFieldName.keySet()) {
            // System.debug(fieldMap.get(eachFieldName));
            // Schema.DescribeFieldResult field = fieldMap.get(eachFieldName).getDescribe();
            // System.debug(field.getType());
            
    }

    public class ObjectSearchResult{
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String label {get; set;}
        
        public ObjectSearchResult(EntityDefinition def) {
            name = def.QualifiedApiName;
            label = def.Label;
        }
    }

    public class FieldResponse{
        @AuraEnabled
        public List<FieldData> fields;
        @AuraEnabled
        public Map<String, String> inputTypeByFieldName;
        // @AuraEnabled
        // public Map<String, String> fieldTypeByName;
        // @AuraEnabled
        // public Map<String, String> fieldLabelByName;
    }

    public class FieldData implements Comparable{
        @AuraEnabled
        public String name;
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String inputType;
        @AuraEnabled
        public Object previousValue;
        @AuraEnabled
        public Boolean isRequired;
        @AuraEnabled
        public Boolean isUnique;
        @AuraEnabled
        public Object configuredValue;
        
        public FieldData(Schema.DescribeFieldResult field)
        {
            name = field.getName();
            label = field.getLabel();
            isRequired = !field.isNillable();
            isUnique = field.isUnique();
        }

        public Integer CompareTo(Object ObjToCompare) {
            FieldData that = (FieldData)ObjToCompare;
            if (this.label > that.label) return 1;
            if (this.label < that.label) return -1;
            return 0;
        }
    }
}

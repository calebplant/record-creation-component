({
    doInit : function(component, event, helper) {
        console.log('Doing init ...');
        // console.log(JSON.parse(JSON.stringify(component.get("v.fieldData"))));
        let fieldOptions = component.get("v.fieldData").map(eachField => {
            return { "label": eachField.label, "value": eachField.name }
        });
        component.set("v.fieldOptions", fieldOptions);
        if(component.get("v.fieldName")) {
            let fieldData = component.get("v.fieldData");
            let inputType = fieldData.find(eachField => eachField.name === component.get("v.fieldName")).inputType;
            console.log(inputType);
            component.set("v.inputType", inputType);
        }
    },

    handleSelectedObjChange : function(component, event, helper) {
        console.log('handleSelectedObjChange');
        let selectedName = event.getParam("value");
        component.set("v.fieldName", selectedName);
        let fieldData = component.get("v.fieldData");
        let inputType = fieldData.find(eachField => eachField.name === selectedName).inputType;
        console.log(inputType);
        component.set("v.inputType", inputType);
    },

    handleInputChange : function(component, event, helper) {
        console.log('handleInputChange');
        var name = event.getSource().get("v.name");
        let value;
        switch(name) {
            case 'textInput':
            case 'numberInput':
            case 'dateInput':
            case 'dateTimeInput':
            case 'emailInput':
            case 'telephoneInput':
            case 'urlInput':
                console.log(event.getParam("value"));
                value = event.getParam("value");
                break;
            case 'checkboxInput':
                value = event.getSource().get('v.checked');
                break;
        }
        component.set('v.value', value);
    },

    getRowValues : function(component, event, helper) {
        console.log('getRowValues');
        var params = event.getParam('arguments');
        if (params) {
            // var param1 = params.param1;
            // do stuff here
        }
        return {
            index: component.get("v.index"),
            fieldName: component.get("v.fieldName"),
            value: component.get("v.value")
        };
    },

    handleDeleteRow: function(component, event, helper) {
        console.log('handleDeleteRow');
        var deleteRowEvent = component.getEvent("deleteRowFired");
 
        deleteRowEvent.setParams({
            rowIndex: component.get("v.index")
        }).fire();
    }
})

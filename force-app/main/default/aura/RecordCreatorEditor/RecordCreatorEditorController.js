({
    doInit: function(component, event, helper) {
        console.log('Doing init...');
        // Create the action
        let action = component.get("c.getAvailableObjects");
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                console.log('Fetched available objects');
                console.log('Response from server: ');
                console.log(response.getReturnValue());
                component.set("v.objectOptions", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);
    },

    handleLookupSearch: function(component, event, helper) {
        console.log('handleLookupSearch');
        var target = event.getSource();
        let searchTerm = event.getParam("searchTerm");
        console.log('searchTerm');
        console.log(searchTerm);

        let objectOptions = component.get("v.objectOptions");
        let searchResults = objectOptions.filter(eachOption => {
            return eachOption.label.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0;
        });
        console.log(searchResults);
        target.setSearchResults(searchResults);      
    },

    handleObjectLookupChange: function(component, event, helper) {
        console.log('handleObjectLookupChange');
        let selectedObject = event.getParam("selection")[0];
        // console.log('selectedObject:');
        // console.log(event.getParam("selection")[0]);
        component.set("v.selectedObject", selectedObject);
        // const selectedName = event.detail;
        // console.log(JSON.parse(JSON.stringify(selectedName)));

        // this.dispatchEvent(new CustomEvent('updateobjname', {detail: event.detail[0]}));
    },

    handleSelectObject: function(component, event, helper) {
        console.log('handleSelectObject');
        // Create the action
        let action = component.get("c.getReadableObjectFields");
        action.setParams({ objectApiName : component.get("v.selectedObject") });
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                console.log('Fetched available fields');
                console.log('Response from server: ');
                console.log(response.getReturnValue());
                component.set("v.fieldData", response.getReturnValue().fields);
                component.set("v.filteredFieldData", response.getReturnValue().fields);
                console.log('fieldData: ');
                console.log(JSON.parse(JSON.stringify(component.get('v.fieldData'))));

                let startingRows;
                let previousConfig = response.getReturnValue().fields.filter(eachField => {
                    return eachField.previousValue;
                });
                console.log('PREVIOUS CONFIG:');
                console.log(JSON.parse(JSON.stringify(previousConfig)));
                if(previousConfig && previousConfig.length) {
                    let rowIndex = -1;
                    startingRows = previousConfig.map(eachField => {
                        rowIndex++;
                        return {
                            index: rowIndex,
                            fieldName: eachField.name,
                            value: eachField.previousValue
                        }
                    })
                } else {
                    startingRows = [{
                        index: 0,
                        fieldName: '',
                        value: ''
                    }];
                }

                component.set("v.fieldRowValues", startingRows);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);
    },

    handleFieldDataUpdate: function(component, event, helper) {
        console.log('handleFieldDataUpdate');
        // let requiredFields = component.get("v.fieldData")['fields'].filter(eachField => eachField.isRequired == true);
        // console.log('Required Fields:');
        // console.log(requiredFields);
        // component.set("v.requiredFields", requiredFields);

        // let requiredFields = component.get("v.fieldData")['fields'].filter(eachField => eachField.isRequired == true);
        // console.log('Required Fields:');
        // console.log(requiredFields);
        // component.set("v.requiredFields", requiredFields);

        // TODO: pull up stored metadata and set previously defined values

    },

    handleFieldDataUpdate: function(component, event, helper) {
        
    },

    handleChangeTester:  function(component, event, helper) {
        console.log('handleChangeTester');
        console.log(JSON.parse(JSON.stringify(component.get("v.fieldRowValues"))));
    },

    handleTester:  function(component, event, helper) {
        console.log('handleTester');
        // console.log(JSON.parse(JSON.stringify(component.get("v.fieldRowValues"))));
        let rows = component.find('editorRows');
        // console.log('rows:');
        // console.log(JSON.parse(JSON.stringify(rows)));
        let rowValues;
        if(Array.isArray(rows)) {
            console.log('isArray');
            rowValues = rows.map(eachRow => {
                return eachRow.getRowValues();
            })
            console.log(JSON.parse(JSON.stringify(rowValues)));
        } else {
            console.log('NOT array');
            rowValues = rows.getRowValues();
            console.log(JSON.parse(JSON.stringify(rowValues)));
        }
    },

    handleAddRow: function(component, event, helper) {
        console.log('handleAddRow');
        let updatedRows = component.get('v.fieldRowValues');
        updatedRows.push(
            {
                index: updatedRows.length,
                fieldName: '',
                value: ''
            }
        )
        component.set("v.fieldRowValues", updatedRows);
    },

    handleDeleteRow: function(component, event, helper) {
        console.log('handleDeleteRow');
        let deleteIndex = event.getParam("rowIndex");
        console.log('deleteIndex: ' + deleteIndex);

        let rows = component.find('editorRows');
        let rowValues;
        if(Array.isArray(rows)) {
            console.log('isArray');
            rowValues = rows.map(eachRow => {
                return eachRow.getRowValues();
            })
            console.log(JSON.parse(JSON.stringify(rowValues)));
        } else {
            console.log('NOT array');
            rowValues = rows.getRowValues();
            console.log(JSON.parse(JSON.stringify(rowValues)));
        }

        let newRowIndex = 0;
        let updatedRowValues = rowValues.map(eachRow => {
            // Update row index
            if(eachRow.index != deleteIndex) {
                eachRow.index = newRowIndex;
                newRowIndex++;
                return eachRow;
            }
        })
        .filter(eachRow => {
            // Remove deleted row
            return eachRow != null;
        });
        console.log('updatedRowValues');
        console.log(JSON.parse(JSON.stringify(updatedRowValues)));

        component.set("v.fieldRowValues", updatedRowValues);
    },

    handleSaveChanges: function(component, event, helper) {
        try {
            console.log('handleSaveChanges');   
            let objectName = component.get("v.selectedObject");
    
            let rows = component.find('editorRows');
            let rowValues = {};
            if(Array.isArray(rows)) {
                console.log('isArray');
                rows.forEach(eachRow => {
                    let values = eachRow.getRowValues();
                    rowValues[values.fieldName] = values.value;
                })
                console.log(JSON.parse(JSON.stringify(rowValues)));
            } else {
                console.log('NOT array');
                rowValues = rows.getRowValues();
                console.log(JSON.parse(JSON.stringify(rowValues)));
            }
    
            let action = component.get("c.updateConfig");
            action.setParams({ objectName : objectName, fieldsJsonStr: JSON.stringify(rowValues) });
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (state === "SUCCESS") {
                    console.log('Successfully updated metadata config');
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Updated",
                        "message": "Successfully updated metadata",
                        "type": "success"
                    });
                    resultsToast.fire();
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            // Send action off to be executed
            $A.enqueueAction(action);
            console.log('end handleSaveChanges');  
        } catch(err) {
            console.log(err);
        }
         
    }
})

({
    doInit: function(component, event, helper) {
        console.log('Doing init...');
    },

    // Handlers
    handleObjectSelected: function (component, event, helper) {
        // This will contain the string of the "value" attribute of the selected option
        var selectedOptionValue = event.getParam("value");
        console.log('Selected: ' + selectedOptionValue);
        component.set("v.selectedObject", selectedOptionValue);

        component.set("v.isSubmitDisabled", true);

        try {
            let fieldData = JSON.parse(component.get("v.metaDataRecord.Field_Data__c"))[selectedOptionValue];
            component.set("v.selectedObjectFieldData", JSON.stringify(fieldData[selectedOptionValue]));
            let fields = [];
            for (const [key, value] of Object.entries(fieldData)) {
                fields.push(key);
            }
            // console.log(fields);
            component.set("v.recordFields", fields);
            // Prepare a new record from template
            // let x = component.get("v.recordFields");
            // console.log('x: ');
            // console.log(x);
            component.find("recordCreator").getNewRecord(
                selectedOptionValue, // sObject type (objectApiName)
                null,      // recordTypeId
                false,     // skip cache?
                $A.getCallback(function() {
                    var rec = component.get("v.newObject");
                    var error = component.get("v.newRecordError");
                    if(error || (rec === null)) {
                        console.log("Error initializing record template: " + error);
                        return;
                    }
                    console.log("Record template initialized: " + rec.apiName);
                    component.set("v.isSubmitDisabled", false);
                })
            );
        } catch(err) {
            console.log(err);
        }

    },

    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            console.log("Record loaded successfully.");
            let fieldData = JSON.parse(component.get("v.metaDataRecord.Field_Data__c"));
            let objectOptions = [];
            for (const [key, value] of Object.entries(fieldData)) {
                objectOptions.push({
                    label: key,
                    value: key
                });
              }
            objectOptions.sort((a, b) => (a.label > b.label) ? 1 : -1);
            console.log(objectOptions);
            component.set("v.objectOptions", objectOptions);
            // component.set("v.selectedObject", objectOptions[0].value);
            
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    },

    handleCreateRecord: function(component, event, helper) {
        console.log('handleCreateRecord');
        let fieldData = JSON.parse(component.get("v.metaDataRecord.Field_Data__c"))[component.get("v.selectedObject")];
        console.log('fieldData:');
        console.log(fieldData);
        let newRecordFieldValues = component.get("v.simpleNewObject");
        for (const [key, value] of Object.entries(fieldData)) {
            newRecordFieldValues[key] = value;
        }
        console.log('result: ');
        console.log(JSON.parse(JSON.stringify(newRecordFieldValues)));

        component.find("recordCreator").saveRecord(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                // record is saved successfully
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The record was saved.",
                    "type": "success"
                });
                resultsToast.fire();

            } else if (saveResult.state === "INCOMPLETE") {
                // handle the incomplete state
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                // handle the error state
                console.log('Problem saving contact, error: ' + JSON.stringify(saveResult.error));
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Error",
                    "message": "The record was NOT saved: " + JSON.stringify(saveResult.error),
                    "type": "error"
                });
                resultsToast.fire();
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        });
    }
});
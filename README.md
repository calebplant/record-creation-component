# Record Creation Component

## Overview

An aura component allowing users to create records with the click of a button. The record's field values are based on configured custom metadata. This org also contains a component that is used for editing the custom metadata.

## Demo (~1 min)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=nwwJv2O3zlc)

## Some Screenshots

### Creating a Record

![Creating a Record](media/record-creation.png)

Users select from objects that have been configured in the custom metadata.

### Metadata Editor

![Metadata Editor](media/metadata-editor.png)

This editor assists the user in configuring default field values (by editing the custom metadata). Once the user submits their changes, it essentially translates the input into a JSON that is stored and later used by the creation component.

#### Custom Metadata JSON Example
![Custom Metadata](media/custom-metadata.png)
